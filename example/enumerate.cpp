
#include <iostream>
#include <string>
#include <vector>
#include "pythonicpp/pythonicpp.h"

class Thing {
 public:
  Thing(const std::string name): name_(name) {}
  const std::string name_;
};

int main() {
  // using namespace pythonicpp;
  std::cout << "Hello World\n";

  std::vector<Thing> things;
  things.push_back(Thing("0"));
  things.push_back(Thing("1"));
  things.push_back(Thing("2"));

  for (auto [i, thing] : pythonicpp::enumerate(things)) {
    // i gets the index and thing gets the Thing in each iteration
    std::cout << "Thing " << i << " has name " << thing.name_ << " \n";
  }
}