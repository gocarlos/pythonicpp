# PythoniCPP

## build instructions

```bash
mkdir _build
cd _build
cmake ..
make -j
```

## pythonic functions

* enumerate - http://reedbeta.com/blog/python-like-enumerate-in-cpp17/

```cpp
  std::vector<Thing> things;
  things.push_back(Thing("0"));
  things.push_back(Thing("1"));
  things.push_back(Thing("2"));

  for (auto [i, thing] : pythonicpp::enumerate(things)) {
    // i gets the index and thing gets the Thing in each iteration
    std::cout << "Thing " << i << " has name " << thing.name_ << " \n";
  }
```

* length 
```cpp
// to be done
```